# .gitlab-ci.yaml tips

- Use the [pipeline editor](https://docs.gitlab.com/ee/ci/pipeline_editor/index.html) to edit .gitlab-ci.yml at **Build > Pipeline editor**.
    - This option is only visible by the developers of this project.
- Artifacts are sent to GitLab and can be viewed at **Build > Artifact**.

# Takeaways

- `stages` defines a pipeline.
    - Without being defined, it defaults to

```yaml
stages:
  - build
  - test
  - deploy
```

- `stage` is default to `test` if not assigned.
- 其實不是所有的任務都要跟 codebase 有關，pipeline 甚至可以僅僅作為 CLI tools 的圖形化操作工具，像是定期更新 staging 站台的 aetherSlide。

# TODO

- Scheduled pipelines.
